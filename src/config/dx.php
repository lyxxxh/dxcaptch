<?php

return [


    //appid
    'appId' => env('DX_APP_ID'),

    //密匙
    'appSec' => env('DX_APP_SEC'),


    //设置超时时间，默认2秒
    'timeOut' => 2,


    //文档: 确保验证状态是SERVER_SUCCESS，SDK中有容错机制，在网络出现异常的情况会返回通过
    //'strict'=true的话　网络异常就返回false
    'strict' => true,

    //文档: 特殊情况可以额外指定服务器，默认情况下不需要设置$client->setCaptchaUrl("http://cap.dingxiang-inc.com/api/tokenVerify");
    //可设置'set_captcha_url'=>'http://cap.dingxiang-inc.com/api/tokenVerify'
    'set_captcha_url' => null

];
