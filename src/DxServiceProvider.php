<?php

namespace Xxh\Dx;

use Illuminate\Support\ServiceProvider;

class DxServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/config/dx.php', 'dx'
        );
    }


    public function boot()
    {
        $this->publishes([
            __DIR__.'/config/dx.php' => config_path('dx.php'),
        ]);
    }




}
