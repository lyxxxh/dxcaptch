<?php

namespace Xxh\Dx;


use Illuminate\Support\Facades\Log;
use Xxh\Dx\Sdk\CaptchaClient;

trait DxCaptChService
{



    /*
     *@param $token 顶像token
     * @return bool
     */
    public function dxVerify($token)
    {
        $config = config('dx');
        $client = new CaptchaClient($config['appId'],$config['appSec']);
        $client->setTimeOut($config['timeOut']);

        if(! is_null($config['set_captcha_url']))
        $client->setCaptchaUrl($config['set_captcha_url']);

        $response = $client->verifyToken($token);

        if( $response->serverStatus != 'SERVER_SUCCESS' && $config['strict']) //网络异常 并且是严格模式　就验证失败
        return false;

        return $response->result;
    }





}
