### 简介

laravel[顶象验证](https://www.dingxiang-inc.com/docs/detail/captcha#doc-h3-31)的封装


### 安装

`composer require lyxxxh/dxcaptch`


### 配置

1. `config/app.php`　（Laravel 5.5 + 无需手动注册）

```
'providers' => [
    // ...
    Xxh\Dx\DxServiceProvider::class,
],
```


2.  ``` php artisan vendor:publish --provider="Xxh\Dx\DxServiceProvider" ```

3. 修改config/dx.php的appId和appSec为你的配置


### 使用

```

class UserController  extends Controller
{

    //使用顶象验证
    use DxCaptChService;
    
    public function Login(Request $request)
    {
    
        //$request->token为前端滑块成功后得到的token
        if( $this->dxVerify($request->token))
          echo "验证成功";
        else
          echo "失败";  
          
    }
    
    
    
```
    
        

